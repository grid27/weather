import React, { Component } from 'react';
import { Provider } from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import store from './store';
import router from './router';
import './styles/main.css';


class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <MuiThemeProvider>
          {router}
        </MuiThemeProvider>
      </Provider>
    );
  }
}

export default App;