import _ from 'lodash';
import { WEATHER } from '../types/weather-types'

const { WEATHER_API_KEY, GOOGLE_MAP_API_KEY } = require('../config.json');


export default class Urls {

  /**
   * @description Api для получения погоды на 5 дней
   * @type Function
   * @param {latitude, longitude}:Object - Широта, Долгота 
   * @param type:String - тип получаемых данных `на текущий день` по умолчанию
   * @return String
   */
  static weatherApi({latitude, longitude}, type=WEATHER) {
    let param = Urls.joinParam({
      lat: latitude,
      lon: longitude,
      units: 'metric',
      appid: WEATHER_API_KEY
    });
    return `http://api.openweathermap.org/data/2.5/${type}?${param}`;
  }

  /**
   * @description Api для получения координат по названию города
   * @type Function
   * @param cityName:String - Название города
   * @return String
   */
  static geoDecoderApiByName(cityName) {
    let param = Urls.joinParam({
      address: cityName,
      result_type:'locality',
      language:'RU',
      key: GOOGLE_MAP_API_KEY
    });
    return encodeURI(`${Urls.geoDecoderApi}?${param}`);
  }

  /**
   * @description Api для получения Города по координатам
   * @type Function
   * @param {latitude, longitude}:Object - Широта, Долгота
   * @return String
   */
  static geoDecoderApiByCoords({latitude, longitude}) {
    let param = Urls.joinParam({
      latlng:`${latitude},${longitude}`,
      result_type:'locality',
      language:'RU',
      key: GOOGLE_MAP_API_KEY
    });
    return `${Urls.geoDecoderApi}?${param}`
  }

  static geoDecoderApi = 'https://maps.google.com/maps/api/geocode/json';

  static joinParam(param) {
    return _.entries(param).map(([k,v])=>{return `${k}=${v}`}).join("&")
  }
}