import React, { Component } from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';

import AppBar from 'material-ui/AppBar';
import { Card, CardActions, CardHeader, CardText } from 'material-ui/Card';

import WeatherService from '../services/weather-service';
import * as weatherTypes from '../types/weather-types';
import {addCity, rmCity, addCities} from '../actions/weather-actions';

import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import AutoComplete from 'material-ui/AutoComplete';

const { DEFAULT_CITY } = require("../config.json");


class Weather extends Component {

  state = {
    openModal: false,
    foundCities: []
  };

  _getCurrentCity() {
    return new Promise(resolve => {
      const errorGeoLocation = () => {
        WeatherService.getCities(DEFAULT_CITY).then(cities => {
          resolve(cities[0]);
        });
      };

      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
          (pos) => {
            let {latitude, longitude} = pos.coords;
            WeatherService.getCities({latitude, longitude}).then(cities => {
              resolve(cities[0]);
            });
          },
          (err) => {
            errorGeoLocation();
            console.error(err);
          }
        );
      }
      else {
        errorGeoLocation();
      }
    })
  }

  async _getSavedCity() {
    const savedSities = JSON.parse(localStorage.getItem('cities'));
    if (savedSities && savedSities.length) {
      return await Promise.all(
        savedSities.map(async (city) => {
          return await WeatherService.getWeather(city);
        })
      );
    }
    else {
      return false;
    }
  }

  modalWin() {
    const actions = [
      <FlatButton
        label="Закрыть"
        primary={true}
        keyboardFocused={true}
        onClick={this.closeModalWin.bind(this)}
      />,
    ];
    return (
      <Dialog
        title="Поиск города"
        actions={actions}
        modal={true}
        open={this.state.openModal}
        onRequestClose={this.closeModalWin.bind(this)}
      >
        <AutoComplete
          floatingLabelText="Название города"
          dataSource={_.map(this.state.foundCities, x => {
            return x.formatted_address
          })}
          onUpdateInput={this.handleUpdateFindCity.bind(this)}
          fullWidth={true}
          onNewRequest={this.selectCity.bind(this)}
        />
      </Dialog>
    )
  }

  selectCity(value) {
    let city = _.find(this.state.foundCities, x => {
      return x.formatted_address === value
    });
    this.addCity(city);
    this.closeModalWin();
  }

  async handleUpdateFindCity(value) {
    if (value.length > 3) {
      const cities = await WeatherService.getCities(value);
      this.setState({foundCities: cities || []});
    }
  }

  showModalWin() {
    this.setState({openModal: true});
  }

  closeModalWin() {
    this.setState({openModal: false});
  }

  async addCity(city) {
    city = await WeatherService.getWeather(city);
    addCity(city);
  }

  async componentDidMount() {
    let cities = await this._getSavedCity();
    if (cities) {
      addCities(cities);
    }
    else {
      let currentCity = await this._getCurrentCity();
      this.addCity(currentCity);
    }
  }

  renderCityWeather(city, index) {
    let weather = city.weather[weatherTypes.WEATHER];
    if (weather) {
      let {temp} = weather.main;
      temp = temp >= 0 ? "+" + temp : temp;
      return (
        <Card key={index.toString()}>
          <CardHeader
            title={city.cityName}
            titleStyle={{fontSize: 26}}
            subtitle={city.regionName}
            subtitleStyle={{fontSize: 18}}
          />
          <CardText>
            <p style={{fontSize: 22, fontWeight: "bold"}}>{`${temp} °C`}</p>
          </CardText>
          <CardActions>
            <FlatButton label="Удалить" onClick={() => rmCity(city)}/>
          </CardActions>
        </Card>
      )
    }
    else {
      return null;
    }
  }

  render() {
    return (
      <main>
        <AppBar
          title="Погода"
          showMenuIconButton={false}
          iconElementRight={
            <FlatButton
              onClick={this.showModalWin.bind(this)}
              label="Добавить город"/>
          }
        />

        {this.props.cities.map((city, index) => {
          return this.renderCityWeather(city, index)
        })}
        {this.modalWin()}
      </main>
    )
  }
}

const mapStateToProps = function (store) {
  return {
    cities: store.weatherState.cities
  }
};

export default connect(mapStateToProps)(Weather);