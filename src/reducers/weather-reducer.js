import _ from 'lodash';

import { ADD_CITY, ADD_CITIES, RM_CITY } from '../types/action-types';


const initialState = {
  cities: []
};

const weatherReducer = function(state = initialState, action) {
    let newState = null;
    let cities = null;

    switch(action.type) {
      case ADD_CITY:
        cities = [action.city].concat(state.cities);
        localStorage.setItem('cities', JSON.stringify(cities));
        newState = Object.assign({}, state, { cities });
        break;
      case ADD_CITIES:
        newState = Object.assign({}, state, { cities: action.cities });
        break;
      case RM_CITY:
        cities = _.filter(state.cities, x => {return x !== action.city});
        localStorage.setItem('cities', JSON.stringify(cities));
        newState = Object.assign({}, state, { cities });
        break;
      default:
        newState = state;
    }
    return newState;
  };
  
  export default weatherReducer;