import { combineReducers } from 'redux';

// Reducers
import weatherReducer from './weather-reducer';

// Combine Reducers
const reducers = combineReducers({
    weatherState: weatherReducer,
});

export default reducers;