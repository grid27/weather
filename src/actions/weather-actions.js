import { ADD_CITY, ADD_CITIES, RM_CITY } from '../types/action-types';
import store from '../store';

export function addCity(city) {
  store.dispatch({type: ADD_CITY, city});
}

export function addCities(cities) {
  store.dispatch({type: ADD_CITIES, cities});
}

export function rmCity(city) {
  store.dispatch({type: RM_CITY, city});
}