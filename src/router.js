import React from 'react';
import { HashRouter as Router, Route } from 'react-router-dom';

import Weather from './components/Weather';


export default (
  <Router>
    <Route exact path="/" component={Weather} />
  </Router>
);