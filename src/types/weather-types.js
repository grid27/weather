export const WEATHER = 'weather';  // Прогноз на текущий день
export const FORECAST = 'forecast'; // Прогноз на 5 дней
export const FORECAST_DAILY = 'forecast/daily';  // Прогноз на 16 дней