import axios from 'axios';
import Urls from '../models/urls';
import { ERROR_GET_WEATHER, ERROR_GET_CITY } from '../types/error-types';
import { WEATHER } from '../types/weather-types';


export default class WeatherService {

  static async getWeather(city, type=WEATHER) {
    // Получение погоды
    try {
      let normalizeType = type.replace('/', '_');
      let response = await axios.get(Urls.weatherApi(city.location, type));
      city.weather[normalizeType] = response.data;
      return city;
    }
    catch(e) {
      console.error(e);
      return ERROR_GET_WEATHER;
    }
  }

  static async getCities(param) {
    // Получение информации о городах
    try {
      let url = (typeof param === "string" 
                  ?Urls.geoDecoderApiByName(param)
                  :Urls.geoDecoderApiByCoords(param));
                  
      const response = await axios.get(url);
      let { results } = response.data;
      let cities = [];
      results.forEach( res => {
        let { lat, lng } = res.geometry.location;
        cities.push({
          cityName: res.address_components[0].long_name,
          regionName: res.address_components[1].long_name,
          country: res.address_components[2].long_name,
          formatted_address: res.formatted_address,
          location : {
            latitude: lat,
            longitude: lng
          },
          weather: {}
        });
      });
      return cities;
    }
    catch(e) {
      console.error(e);
      return ERROR_GET_CITY;
    }
  }

}